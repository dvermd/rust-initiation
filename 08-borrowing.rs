// -----------------------------------------------------------------------------
// Atelier Rust
// 2019-05-23 - 14h -> 16h
// Animateur: Axel Viala (darnuria)
//
// struct.rs
//
// Dans cet exercice on va écrire notre première fonction pour manipuler un
// vecteur et ensuite on découvrira que l'ont peut le faire avec une fonction
// anonyme.
//
//
// Objectifs pédagogiques:
//
// - Utilisation des vecteurs
// - Boucles `for`
// - Fonctions annonymes `closures`
//
// /!\ Quand vous verrez les symboles: `???`, il s'agit de code à
// completer soit-même c'est normal que Rust indique une erreur! :)
// -----------------------------------------------------------------------------

// Étapes:
//
// 0. On fait la somme avec une boucle
// 1. on fait la somme sur un vector!
// 2. On extrait dans une fonction
// 2.1 On utilise une slice plutôt que une reférence sur un vecteur
// 3. On refait avec une closure et une fonction d'ordre supérieur (fold)



fn main() {
    
}